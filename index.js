let arr = [1, 2.45, "word", "hello", true, false, null, undefined];
let typeArrItem = "string"; 

function filterBy(array, type) {

    let newArr = array.filter(function (number) {
        return typeof(number) !== type;
    } );

    return newArr;
}
console.log(arr);
console.log(filterBy(arr, typeArrItem));
